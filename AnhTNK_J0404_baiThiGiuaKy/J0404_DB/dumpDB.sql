-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2021 at 07:34 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_devcamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `chapter`
--

CREATE TABLE `chapter` (
  `id` bigint(20) NOT NULL,
  `introduce_chapter` varchar(255) DEFAULT NULL,
  `ma_chapter` varchar(255) DEFAULT NULL,
  `name_translator_chapter` varchar(255) DEFAULT NULL,
  `page_chapter` varchar(255) DEFAULT NULL,
  `ten_chapter` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chapter`
--

INSERT INTO `chapter` (`id`, `introduce_chapter`, `ma_chapter`, `name_translator_chapter`, `page_chapter`, `ten_chapter`) VALUES
(1, 'Gioi thieu 01', '012333', 'null', '01', 'chapter 012'),
(4, 'Giới thiệu chapter 024', '024', 'null', '24', 'chapter 024'),
(5, 'khong co', '023', 'Nguyen Van B', '23', 'chapter 023'),
(6, 'Gioi thieu 01', '014', 'null', '01', 'chapter 01');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) NOT NULL,
  `introduce_post` varchar(255) DEFAULT NULL,
  `ma_post` varchar(255) DEFAULT NULL,
  `name_post` varchar(255) DEFAULT NULL,
  `page_post` varchar(255) DEFAULT NULL,
  `chapter_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `introduce_post`, `ma_post`, `name_post`, `page_post`, `chapter_id`) VALUES
(2, 'gioi thieu', '064', 'post 03', '03', 1),
(7, 'gioi thieu', '031', 'post 0317', '031', 6),
(18, 'Giới thiệu post 21', '099', 'post 021', '21', 1),
(20, 'gioi thieu', '034', 'post 031', '031', 1),
(21, 'gioi thieu', '0345', 'post 0345', '0345', 1),
(22, 'khong co', '1234', 'post 1234', '123', 6),
(23, 'null', '8988', 'post 8988', '800', 6),
(24, 'null', '9124', 'post 91212', '912', 6),
(25, 'this is post 018', '018', 'post 018', '18', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chapter`
--
ALTER TABLE `chapter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_og0cgkfw78prqwr6u8qc18sgw` (`ma_chapter`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_g2stniy8jb37jesdc59a0q7km` (`ma_post`),
  ADD KEY `FKe8xm50u3soa66uamn97f14otv` (`chapter_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chapter`
--
ALTER TABLE `chapter`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `FKe8xm50u3soa66uamn97f14otv` FOREIGN KEY (`chapter_id`) REFERENCES `chapter` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
