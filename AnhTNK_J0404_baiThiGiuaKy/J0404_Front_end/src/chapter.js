"use strict";
// REGION 1 : 
// Array Chapter 
const gCHAPTER_COL = ["maChapter","tenChapter","nameTranslatorChapter","introduceChapter","pageChapter","action"];

// COL In Arr
const gCHAPTER_CODE_COL = 0;
const gCHAPTER_NAME_COL = 1;
const gCHAPTER_TRANS_COL = 2;
const gCHAPTER_INTRO_COL = 3;
const gCHAPTER_PAGE_COL = 4;
const gCHAPTER_ACTION_COL = 5;


// Data table
let chapterTable = $('#chapter-table').DataTable({
  columns: [
    { data: gCHAPTER_COL[gCHAPTER_CODE_COL]},
    { data: gCHAPTER_COL[gCHAPTER_NAME_COL]},
    { data: gCHAPTER_COL[gCHAPTER_TRANS_COL]},
    { data: gCHAPTER_COL[gCHAPTER_INTRO_COL]},
    { data: gCHAPTER_COL[gCHAPTER_PAGE_COL]},
    { data: gCHAPTER_COL[gCHAPTER_ACTION_COL]},
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
});

let gChapterId = 0;
let chapter = {
  newChapter: {
    maChapter : "",
    tenChapter: "",
    nameTranslatorChapter: "",
    introduceChapter: "",
    pageChapter: ""
  },
  onCreateChapterClick() {
    gChapterId = 0;
    this.newChapter = {
      maChapter : $('#input-chapter-code').val().trim(),
      tenChapter: $('#input-chapter-name').val().trim(),
      nameTranslatorChapter: $('#input-trans').val().trim(),
      introduceChapter: $('#input-intro').val().trim(),
      pageChapter: $('#input-page').val().trim(),
    };
    if (validateChapter(this.newChapter)) {
      $.ajax({
        url: `http://127.0.0.1:8080/chapter/create`,
        method: 'POST',
        data: JSON.stringify(this.newChapter),
        contentType: 'application/json',
        success: () => {
          alert('Chapter created successfully');
          resetChapter();
          getChapterFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onDetailsChapterClick() {
    let vSelectRow = $(this).parents('tr');
    let vSelectedData = chapterTable.row(vSelectRow).data();
    gChapterId = vSelectedData.id;
    console.log(vSelectedData.id);
    $.get(`http://localhost:8080/chapter/details/${gChapterId}`, loadChapterToInput);
  },
  onUpdateChapterClick() {
    this.newChapter = {
      maChapter : $('#input-chapter-code').val(),
      tenChapter: $('#input-chapter-name').val(),
      nameTranslatorChapter: $('#input-trans').val(),
      introduceChapter: $('#input-intro').val(),
      pageChapter: $('#input-page').val(),
    };
    if (validateChapter(this.newChapter)) {
      $.ajax({
        url: 'http://localhost:8080/chapter/update/' + gChapterId,
        method: 'PUT',
        data: JSON.stringify(this.newChapter),
        contentType: 'application/json',
        success: () => {
          alert('Chapter updated successfully');
          resetChapter();
          gChapterId = 0;
          getChapterFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onDeleteIconClick() {
    $('#modal-delete-chapter').modal('show');
    let vSelectRow = $(this).parents('tr');
    let vSelectedData = chapterTable.row(vSelectRow).data();
    gChapterId = vSelectedData.id;
  },
  onDeleteAllChapterClick() {
    $('#modal-delete-chapter').modal('show');
    gChapterId = 0;
  },
  onConfirmDeleteClick() {
    if (gChapterId === 0) {
      $.ajax({
        url: `http://127.0.0.1:8080/chapters`,
        method: 'DELETE',
        success: () => {
          alert('All menu were successfully deleted');
          $('#modal-delete-chapter').modal('hide');
          getChapterFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    } else {
      $.ajax({
        url: `http://127.0.0.1:8080/chapter/delete/${gChapterId}`,
        method: 'DELETE',
        success: () => {
          alert(`chapter with id: ${gChapterId} was successfully deleted`);
          $('#modal-delete-chapter').modal('hide');
          getChapterFromDb();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
};

// REGION 2 : 
// load page
onLoading();

// creata new chapter
$('#create-chapter').click(chapter.onCreateChapterClick);
// btn edit 
$('#chapter-table').on('click', '.fa-edit', chapter.onDetailsChapterClick);
// delete 1 chapter
$('#chapter-table').on('click', '.fa-trash', chapter.onDeleteIconClick);
// update chapter 
$('#update-chapter').click(chapter.onUpdateChapterClick);
// delete all chapter 
$('#delete-all-chapter').click(chapter.onDeleteAllChapterClick);
// confirm 
$('#delete-chapter').click(chapter.onConfirmDeleteClick);

// REGION 3 : 
function onLoading() {
  // get data Chapter
  getChapterFromDb();
}


// REGION 4 : 
function loadChapterToTable(paramChapter) {
  chapterTable.clear();
  chapterTable.rows.add(paramChapter);
  chapterTable.draw();
}

function getChapterFromDb() {
  $.get(`http://localhost:8080/chapters`, loadChapterToTable);
}



function validateChapter(paramChapter) {
  let vResult = true;
  try {
    if (paramChapter.maChapter == '') {
      vResult = false;
      throw 'Mã chapter không được để trống';
    }
    if (paramChapter.tenChapter == '') {
      vResult = false;
      throw 'Đường kính không được để trống';
    }
    
    if (paramChapter.page == '') {
      vResult = false;
      throw 'Số trang không được để trống';
    }
  } catch (e) {
    alert(e);
  }
  return vResult;
}

function loadChapterToInput(paramChapter) {
  $('#input-chapter-code').val(paramChapter.maChapter);
  $('#input-chapter-name').val(paramChapter.tenChapter);
  $('#input-trans').val(paramChapter.nameTranslatorChapter);
  $('#input-intro').val(paramChapter.introduceChapter);
  $('#input-page').val(paramChapter.pageChapter);
}

function resetChapter() {
  $('#input-chapter-code').val("");
  $('#input-chapter-name').val("");
  $('#input-trans').val("");
  $('#input-intro').val("");
  $('#input-page').val("");
}
