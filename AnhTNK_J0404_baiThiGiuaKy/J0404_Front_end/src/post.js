'use strict';
// REGION 1:
// mảng Post chứa các thuộc tính
const gPOST_COL = ["id", "maPost", "namePost", "introducePost", "pagePost", "action"];
// Các cột tương ứng
const gPOST_ID_COL = 0;
const gPOST_CODE_COL = 1;
const gPOST_NAME_COL = 2;
const gPOST_INTRO_COL = 3;
const gPOST_PAGE_COL = 4;
const gPOST_ACTION_COL = 5;

// create table Post
let postTable = $('#post-table').DataTable({
  columns: [
    { data:  gPOST_COL[gPOST_ID_COL]},
    { data:  gPOST_COL[gPOST_CODE_COL]},
    { data:  gPOST_COL[gPOST_NAME_COL]},
    { data:  gPOST_COL[gPOST_INTRO_COL]},
    { data:  gPOST_COL[gPOST_PAGE_COL]},
    { data:  gPOST_COL[gPOST_ACTION_COL]},
    
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
});

// post
let gChapterId = 0;
let gPostId = 0;

let post = {
  newPost: {
    maPost: '',
    namePost: '',
  },
  onCreatePostClick() {
    this.newPost = {
      maPost: $('#input-post-code').val().trim(),
      namePost: $('#input-post-name').val().trim(),
      introducePost: $('#input-post-intro').val().trim(),
      pagePost: $('#input-page').val().trim(),
    };
    gPostId = 0;
    if (validatePost(this.newPost)) {
      $.ajax({
        url: `http://127.0.0.1:8080/postCreate/${gChapterId}`,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(this.newPost),
        success: (post) => {
          alert(
            `Đã tạo thành công post có  code là: ${post.maPost} và tên là: ${post.namePost}`,
          );
          $.get(
            `http://127.0.0.1:8080/chapter/${gChapterId}/post`,
            loadPostToTable,
          );
          resetPostInput();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onEditPostClick() {
    let vSelectedRow = $(this).parents('tr');
    let vSelectedData = postTable.row(vSelectedRow).data();
    gPostId = vSelectedData.id;
    if (gChapterId == 0) {
      alert('xin hãy chọn 1 chapter');
    }else{
      $.get(
        `http://127.0.0.1:8080/postDetails/${gPostId}`,
        loadPostToInput,
      );
    }    
  },
  onUpdatePostClick() {
    this.newPost = {
      maPost: $('#input-post-code').val().trim(),
      namePost: $('#input-post-name').val().trim(),
      introducePost: $('#input-post-intro').val().trim(),
      pagePost: $('#input-page').val().trim(),
    };
    if (validatePost(this.newPost)) {
      $.ajax({
        url: `http://127.0.0.1:8080/chapter/${gChapterId}/postUpdate/${gPostId}`,
        method: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(this.newPost),
        success: (post) => {
          alert(
            `Đã update thành công post có  code là: ${post.maPost} và tên là: ${post.namePost}`,
          );
          $.get(
            `http://127.0.0.1:8080/postAll`,
            loadPostToTable,
          );
          resetPostInput();
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
  onDeletePostByIdClick() {
    $('#modal-delete-post').modal('show');
    let vSelectedRow = $(this).parents('tr');
    let vSelectedData = postTable.row(vSelectedRow).data();
    gPostId = vSelectedData.id;
  },
  onConfirmDeletePostClick() {
    if (gPostId == 0) {
      $.ajax({
        url: `http://127.0.0.1:8080/postAll`,
        method: 'DELETE',
        success: () => {
          alert(`All post were successfully Delete`);
          $('#modal-delete-post').modal('hide');
          $.get(
            `http://127.0.0.1:8080/postAll`,
            loadPostToTable,
          );
        },
        error: (err) => alert(err.responseText),
      });
    } else {
      $.ajax({
        url: `http://127.0.0.1:8080/postDelete/${gPostId}`,
        method: 'DELETE',
        success: () => {
          alert(`Post with id: ${gPostId} were successfully Delete`);
          $('#modal-delete-post').modal('hide');
          $.get(
            `http://127.0.0.1:8080/postAll`,
            loadPostToTable,
          );
        },
        error: (err) => alert(err.responseText),
      });
    }
  },
};
// REGION 2 : 
// hàm load dữ liệu ra table
onLoading();

// Nút tạo post mới khi click
$('#create-post').click(post.onCreatePostClick);
// Nút update lại post khi click
$('#update-post').click(post.onUpdatePostClick);
// Xác nhận xóa
$('#delete-post').click(post.onConfirmDeletePostClick);
// Xem chi tiết post (theo chapter)
$('#post-table').on('click', '.fa-edit', post.onEditPostClick);
// xóa post được click
$('#post-table').on('click', '.fa-trash', post.onDeletePostByIdClick);

// REGION 3 : 
function onLoading() {
  // lấy data chapter
  $.get(`http://127.0.0.1:8080/chapters`, getChapter);
  // lấy data các post
  $.get(`http://127.0.0.1:8080/postAll`, loadPostToTable);
  
  
}

// truy xuất Chapter select
let vChapterSelectElement = $('#select-chapter');
function getChapter(paramChapter) {
  paramChapter.forEach((chapter) => {
    $('<option>', {
      text: chapter.tenChapter,
      value: chapter.id,
    }).appendTo(vChapterSelectElement);
  });

}
// khi gia tri chapter thay doi thi ham onGetChapteChange duoc goi
vChapterSelectElement.change(onGetChapterChange);


// REGION 4 : 

// laod data ra Table
function loadPostToTable(paramPost) {
  postTable.clear();
  postTable.rows.add(paramPost);
  postTable.draw();
}


function onGetChapterChange(event) {
  gChapterId = event.target.value;
  if (gChapterId == 0) {
    $.get(`http://127.0.0.1:8089/postAll`, loadPostToTable);
  } else {
    $.get(
      `http://127.0.0.1:8080/chapter/${gChapterId}/post`,
      loadPostToTable,
    );
  }
}



function loadPostToInput(paramPost) {
  $('#input-post-code').val(paramPost.maPost);
  $('#input-post-name').val(paramPost.namePost);
  $('#input-post-intro').val(paramPost.introducePost);
  $('#input-page').val(paramPost.pagePost);
}

function validatePost(paramPost) {
  let vResult = true;
  try {
    if (gChapterId == 0) {
      vResult = false;
      throw 'xin hãy chọn 1 chapter';
    }
    if (paramPost.maPost == '') {
      vResult = false;
      throw 'Xin hãy cho Post Code';
    }
    if (paramPost.namePost == '') {
      vResult = false;
      throw 'Xin hãy cho biết tên post';
    }
  } catch (e) {
    alert(e);
  }
  return vResult;
}

function resetPostInput() {
  $('#input-post-code').val();
  $('#input-post-name').val();
  $('#input-post-intro').val();
  $('#input-page').val();
}
