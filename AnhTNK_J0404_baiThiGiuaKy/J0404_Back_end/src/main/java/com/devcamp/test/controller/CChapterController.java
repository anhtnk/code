package com.devcamp.test.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.test.model.CChapter;
import com.devcamp.test.repository.IChapterRepository;


@RestController
public class CChapterController {

	
	@Autowired
	private IChapterRepository iChapterRepository;
	
	@CrossOrigin
	@GetMapping("/chapters")
	public ResponseEntity<List<CChapter>> getAllChapter() {
        try {
            List<CChapter> pChaptersList = new ArrayList<CChapter>();

            iChapterRepository.findAll().forEach(pChaptersList::add);

            return new ResponseEntity<>(pChaptersList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@CrossOrigin
	@GetMapping("/chapter/details/{id}")
	public ResponseEntity<Object> getChapterById(@PathVariable( value = "id") Long id){
		Optional<CChapter> chapterDetail = iChapterRepository.findById(id);
		if (chapterDetail.isPresent()) {
			try {
				return new ResponseEntity<>(chapterDetail.get(), HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@PostMapping("/chapter/create")
	public ResponseEntity<Object> createChapter(@RequestBody CChapter cChapter) {
		try {
			return new ResponseEntity<>(iChapterRepository.save(cChapter), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin
	@PutMapping("/chapter/update/{id}")
	public ResponseEntity<Object> updateChapterById(@PathVariable( value = "id")Long id, @RequestBody CChapter cChapter){
		Optional<CChapter> chapterData = iChapterRepository.findById(id);
		if (chapterData.isPresent()) {
			CChapter _chapter = chapterData.get();
			_chapter.setIntroduceChapter(cChapter.getIntroduceChapter());
			_chapter.setMaChapter(cChapter.getMaChapter());
			_chapter.setNameTranslatorChapter(cChapter.getNameTranslatorChapter());
			_chapter.setPageChapter(cChapter.getPageChapter());
			_chapter.setTenChapter(cChapter.getTenChapter());
			_chapter.setPosts(cChapter.getPosts());
			CChapter saveChapter = iChapterRepository.save(_chapter);
			try {
				return new ResponseEntity<>(saveChapter, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@DeleteMapping("/chapter/delete/{id}")
	public ResponseEntity<Object> deleteById(@PathVariable( value = "id") Long id){
		try {
			Optional<CChapter> chapterData = iChapterRepository.findById(id);
			if ( chapterData.isPresent()) {
				iChapterRepository.deleteById(id);
			}
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin
	@DeleteMapping("/chapter/delete/all")
	public ResponseEntity<Object> deleteAll(){
		try {
			iChapterRepository.deleteAll();
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
