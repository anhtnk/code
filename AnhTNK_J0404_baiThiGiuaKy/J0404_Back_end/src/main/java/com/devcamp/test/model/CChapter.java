package com.devcamp.test.model;

import java.util.Set;

import javax.persistence.*;



@Entity
@Table(name = "chapter")
public class CChapter {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "ma_chapter", unique = true)
	private String maChapter;
	
	@Column(name = "ten_chapter")
	private String tenChapter;
	
	@Column(name = "introduce_chapter")
	private String introduceChapter;
	
	@Column(name = "name_translator_chapter")
	private String nameTranslatorChapter;
	
	@Column(name = "page_chapter")
	private String pageChapter;
	
	@OneToMany(targetEntity = CPost.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "chapter_id")
	private Set<CPost> posts;

	

	/**
	 * 
	 */
	public CChapter() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param maChapter
	 * @param tenChapter
	 * @param introduceChapter
	 * @param nameTranslatorChapter
	 * @param pageChapter
	 * @param posts
	 */
	public CChapter(Long id, String maChapter, String tenChapter, String introduceChapter, String nameTranslatorChapter,
			String pageChapter, Set<CPost> posts) {
		super();
		this.id = id;
		this.maChapter = maChapter;
		this.tenChapter = tenChapter;
		this.introduceChapter = introduceChapter;
		this.nameTranslatorChapter = nameTranslatorChapter;
		this.pageChapter = pageChapter;
		this.posts = posts;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the maChapter
	 */
	public String getMaChapter() {
		return maChapter;
	}

	/**
	 * @param maChapter the maChapter to set
	 */
	public void setMaChapter(String maChapter) {
		this.maChapter = maChapter;
	}

	/**
	 * @return the tenChapter
	 */
	public String getTenChapter() {
		return tenChapter;
	}

	/**
	 * @param tenChapter the tenChapter to set
	 */
	public void setTenChapter(String tenChapter) {
		this.tenChapter = tenChapter;
	}

	/**
	 * @return the introduceChapter
	 */
	public String getIntroduceChapter() {
		return introduceChapter;
	}

	/**
	 * @param introduceChapter the introduceChapter to set
	 */
	public void setIntroduceChapter(String introduceChapter) {
		this.introduceChapter = introduceChapter;
	}

	/**
	 * @return the nameTranslatorChapter
	 */
	public String getNameTranslatorChapter() {
		return nameTranslatorChapter;
	}

	/**
	 * @param nameTranslatorChapter the nameTranslatorChapter to set
	 */
	public void setNameTranslatorChapter(String nameTranslatorChapter) {
		this.nameTranslatorChapter = nameTranslatorChapter;
	}

	/**
	 * @return the pageChapter
	 */
	public String getPageChapter() {
		return pageChapter;
	}

	/**
	 * @param pageChapter the pageChapter to set
	 */
	public void setPageChapter(String pageChapter) {
		this.pageChapter = pageChapter;
	}

	/**
	 * @return the posts
	 */
	public Set<CPost> getPosts() {
		return posts;
	}

	/**
	 * @param posts the posts to set
	 */
	public void setPosts(Set<CPost> posts) {
		this.posts = posts;
	}
	
	
	
	
	
}
