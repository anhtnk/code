package com.devcamp.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDevcampApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestDevcampApplication.class, args);
	}

}
