package com.devcamp.test.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "posts")
public class CPost {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "ma_post", unique = true)
	private String maPost;
	
	@Column(name = "name_post")
	private String namePost;
	
	@Column(name = "introduce_post")
	private String introducePost;
	
	@Column(name = "page_post")
	private String pagePost;
	
	
	@ManyToOne
	@JsonIgnore
    private CChapter chapter;

	/**
	 * 
	 */
	public CPost() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param maPost
	 * @param namePost
	 * @param introducePost
	 * @param pagePost
	 * @param chapter
	 */
	public CPost(Long id, String maPost, String namePost, String introducePost, String pagePost, CChapter chapter) {
		super();
		this.id = id;
		this.maPost = maPost;
		this.namePost = namePost;
		this.introducePost = introducePost;
		this.pagePost = pagePost;
		this.chapter = chapter;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the maPost
	 */
	public String getMaPost() {
		return maPost;
	}

	/**
	 * @param maPost the maPost to set
	 */
	public void setMaPost(String maPost) {
		this.maPost = maPost;
	}

	/**
	 * @return the namePost
	 */
	public String getNamePost() {
		return namePost;
	}

	/**
	 * @param namePost the namePost to set
	 */
	public void setNamePost(String namePost) {
		this.namePost = namePost;
	}

	/**
	 * @return the introducePost
	 */
	public String getIntroducePost() {
		return introducePost;
	}

	/**
	 * @param introducePost the introducePost to set
	 */
	public void setIntroducePost(String introducePost) {
		this.introducePost = introducePost;
	}

	/**
	 * @return the pagePost
	 */
	public String getPagePost() {
		return pagePost;
	}

	/**
	 * @param pagePost the pagePost to set
	 */
	public void setPagePost(String pagePost) {
		this.pagePost = pagePost;
	}

	/**
	 * @return the chapter
	 */
	public CChapter getChapter() {
		return chapter;
	}

	/**
	 * @param chapter the chapter to set
	 */
	public void setChapter(CChapter chapter) {
		this.chapter = chapter;
	}
	
	
	
	
}
