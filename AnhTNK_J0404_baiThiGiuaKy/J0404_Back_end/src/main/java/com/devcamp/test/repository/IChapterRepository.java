package com.devcamp.test.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.test.model.CChapter;

public interface IChapterRepository extends JpaRepository<CChapter, Long>{
	
}
