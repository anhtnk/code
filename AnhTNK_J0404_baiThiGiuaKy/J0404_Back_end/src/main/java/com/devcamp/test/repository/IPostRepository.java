package com.devcamp.test.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.test.model.CPost;

public interface IPostRepository extends JpaRepository<CPost, Long>{
	List<CPost> findByChapterId(Long chapterId);
	Optional<CPost> findByIdAndChapterId(Long id, Long instructorId);
}
