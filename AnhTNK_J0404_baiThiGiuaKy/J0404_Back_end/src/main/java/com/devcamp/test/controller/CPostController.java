package com.devcamp.test.controller;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.test.model.CChapter;
import com.devcamp.test.model.CPost;
import com.devcamp.test.repository.IChapterRepository;
import com.devcamp.test.repository.IPostRepository;

@RestController
public class CPostController {

	
	@Autowired
	private IChapterRepository iChapterRepository;
	
	@Autowired
	private IPostRepository iPostRepository;
	
	@CrossOrigin
	@GetMapping("postAll")
	public ResponseEntity<List<CPost>> getAllPost() {
        try {
            List<CPost> pPostsList = new ArrayList<CPost>();

            iPostRepository.findAll().forEach(pPostsList::add);

            return new ResponseEntity<>(pPostsList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@CrossOrigin
	@GetMapping("postDetails/{id}")
	public ResponseEntity<Object> getPostById(@PathVariable( value = "id") Long id){
		Optional<CPost> postDetail = iPostRepository.findById(id);
		if (postDetail.isPresent()) {
			try {
				return new ResponseEntity<>(postDetail.get(), HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@GetMapping("chapter/{chapterId}/post")
	public ResponseEntity<Object> getPostByChapterId(@PathVariable( value = "chapterId") Long chapterId){
		Optional<CChapter> chapterDetail = iChapterRepository.findById(chapterId);
		if (chapterDetail.isPresent()) {
			try {
				return new ResponseEntity<>(iPostRepository.findByChapterId(chapterId), HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@GetMapping("/chapter/{chapterId}/post/{id}")
	public ResponseEntity<Object> getPostByIdAndChapterId(@PathVariable( value = "chapterId") Long chapterId,@PathVariable( value = "id") Long id){
		Optional<CChapter> chapterDetail = iChapterRepository.findById(chapterId);
		if (chapterDetail.isPresent()) {
			try {
				Optional<CPost> postDetail = iPostRepository.findByIdAndChapterId(chapterId, id);
				return new ResponseEntity<>(postDetail, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	
	@CrossOrigin
	@PostMapping("postCreate/{id}")
	public ResponseEntity<Object> createPost(@PathVariable("id") Long id, @RequestBody CPost cPost){
		try {
			Optional<CChapter> chapterData = iChapterRepository.findById(id);
			if (chapterData.isPresent()) {
				CPost newPost = new CPost();
				newPost.setIntroducePost(cPost.getIntroducePost());
				newPost.setMaPost(cPost.getMaPost());
				newPost.setNamePost(cPost.getNamePost());
				newPost.setPagePost(cPost.getPagePost());
				newPost.setChapter(cPost.getChapter());
				// get Chapter 
				CChapter _chapter = chapterData.get();
				newPost.setChapter(_chapter);
				
				// save data post 
				CPost savedPost = iPostRepository.save(newPost);
				return new ResponseEntity<>(savedPost, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Drink: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	
	}
	
	
	
	@CrossOrigin
	@PutMapping("chapter/{chapterId}/postUpdate/{id}")
	public ResponseEntity<Object> updatePost(@PathVariable("chapterId") Long chapterId,@PathVariable("id") Long id, @RequestBody CPost cPost) {
		Optional<CChapter> chapterData = iChapterRepository.findById(chapterId);
		Optional<CPost> postData = iPostRepository.findById(id);
		if (chapterData.isPresent()) {
			if(postData.isPresent()) {
				CPost _post = postData.get();
				_post.setMaPost(cPost.getMaPost());
				_post.setIntroducePost(cPost.getIntroducePost());
				_post.setNamePost(cPost.getNamePost());
				_post.setPagePost(cPost.getPagePost());
				_post.setChapter(cPost.getChapter());
				CChapter _chapter = chapterData.get();
				_post.setChapter(_chapter);
				// save data post 
				CPost savedPost = iPostRepository.save(_post);
				return new ResponseEntity<>(savedPost, HttpStatus.OK);
			}			
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@CrossOrigin
	@DeleteMapping("postDelete/{id}")
	public ResponseEntity<Object> deletePostById(@PathVariable Long id) {
		try {
			iPostRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
